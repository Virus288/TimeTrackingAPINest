import * as mongoose from 'mongoose';

import { UserSchema } from "../../users/schema/users.schema"
import { TaskSchema } from '../../tasks/schema/tasks.schema';

export const TeamSchema = new mongoose.Schema(
  {
    users: {
      type: [UserSchema],
      required: false,
      default: null,
    },
    tasks: {
      type: [TaskSchema],
      required: false,
      default: null,
    },
    finishedTasks: {
      type: [String],
      required: false,
      default: null,
    }
  },
  { timestamps: false },
);
