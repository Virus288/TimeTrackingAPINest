import { CreateUserDto } from '../../users/DTO/create-user.dto'
import { CreateTaskDto } from '../../tasks/DTO/create-tasks.dto'

export class CreateTeamDto {
  readonly users?: CreateUserDto;
  readonly task?: CreateTaskDto;
  readonly finishedTasks?: Array<string>;
}
