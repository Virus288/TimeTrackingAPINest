import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Put,
  HttpStatus,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { CreateTeamDto } from './DTO/create-teams.dto';
import { Team } from './interface/teams.interface';

import { TeamsService } from './teams.service';

@Controller('teams')
export class TeamsController {
  constructor(private readonly teamService: TeamsService) {}

  @Get()
  async getAll(
    @Res({ passthrough: true }) response: Response,
  ): Promise<Team[] | unknown> {
    return this.errorHandler(this.teamService.findAll(), response);
  }

  @Get(':id')
  async getOne(
    @Param('id') id: string,
    @Res({ passthrough: true }) response: Response,
  ): Promise<Team | unknown> {
    return this.errorHandler(await this.teamService.findOne(id), response);
  }

  @Post()
  create(@Body() createTeamDto: CreateTeamDto): Promise<Team | unknown> {
    return this.teamService.create(createTeamDto);
  }

  @Delete(':id')
  async remove(
    @Param('id') id: string,
    @Res({ passthrough: true }) response: Response,
  ): Promise<Team | unknown> {
    return this.errorHandler(await this.teamService.delete(id), response);
  }

  @Put(':id')
  async update(
    @Body() createTeamDto: CreateTeamDto,
    @Param('id') id: string,
    @Res({ passthrough: true }) response: Response,
  ): Promise<Team | unknown> {
    return this.errorHandler(
      await this.teamService.update(id, createTeamDto),
      response,
    );
  }

  // Handle errors
  errorHandler = (
    data: unknown,
    response: Response<any, Record<string, any>>,
  ) => {
    if (data === null) {
      response.status(HttpStatus.NOT_FOUND).send('Data not found');
    } else {
      return data;
    }
  };
}
