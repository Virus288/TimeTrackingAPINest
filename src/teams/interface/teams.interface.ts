import { User } from '../../users/interface/user.interface';
import { Task } from '../../tasks/interface/tasks.interface';

export interface Team {
  readonly users?: User;
  readonly tasks?: Task,
  finishedTasks?: Array<string>;
}
