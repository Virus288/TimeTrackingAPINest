// External imports
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

// Interface
import { Team } from './interface/teams.interface';

// Validation data
import { userData } from "../users/interface/user.interface"
import { taskData } from "../tasks/interface/tasks.interface"

@Injectable()
export class TeamsService {
  private errors: { [key: string]: string } = {};
  constructor(@InjectModel('Team') private readonly teamModel: Model<Team>) {}

  async findAll(): Promise<Team[]> {
    try {
      return await this.teamModel.find();
    } catch (err) {
      return err;
    }
  }

  async findOne(id: string): Promise<Team | unknown> {
    try {
      return await this.teamModel.findOne({ _id: id });
    } catch (err) {
      console.log(err)
      return this.getErrors(err, 'oneElement');
    }
  }

  async create(team: Team): Promise<Team | unknown> {
    if(team.tasks){
      this.checkIfDataExist(team.tasks, taskData, "task")
    }
    if(team.users){
      this.checkIfDataExist(team.users, userData, "user")
    }
    if(Object.keys(this.errors).length > 0){
      return this.errors
    }
    try {
      const newTeam = new this.teamModel(team);
      return await newTeam.save();
    } catch (err) {
      console.log(err)
      return this.getErrors(err, 'mulipleElements');
    }
  }

  async delete(id: string): Promise<Team | unknown> {
    try {
      return await this.teamModel.findByIdAndRemove({ _id: id });
    } catch (err) {
      return this.getErrors(err, 'oneElement');
    }
  }

  async update(id: string, user: Team): Promise<Team | unknown> {
    try {
      let data = await this.teamModel.findByIdAndUpdate(id, user, { new: true });
      return data
    } catch (err) {
      return this.getErrors(err, 'oneElement');
    }
  }

  // Add new item to existing array
  async addToArray(id: string, user: Team): Promise<Team | unknown> {
    try {
      let data = await this.teamModel.findByIdAndUpdate(id, { $push: { user } }, { new: true });
      return data
    } catch (err) {
      return this.getErrors(err, 'oneElement');
    }
  }

  // Handle errors
  private getErrors = (err: any, from: string) => {
    switch (from) {
      case 'mulipleElements':
        Object.keys(err.errors).forEach((error) => {
          this.addError(error, err.message);
        });
        break;
      case 'oneElement':
        if (err.name === 'CastError') {
          this.addError(err.name,
            err.kind + ' has wrong format. Make sure that you are providing proper data format',
          );
        }
        break;
      default:
        break;
    }
    return { error: true, data: this.errors };
  };

  // Add error to class variables
  private addError(key: string, val: string) {
    this.errors[key] = val;
  }

  // Check if everything required exists inside of data object before adding it to db
  private checkIfDataExist = (data: Object, requiredData: Array<string>, type: string) => {
    requiredData.forEach((item) => {
      if (!Object.keys(data).includes(item)) {
        this.addError(item, `Property '${item}' is missing on type ${type}`);
      }
    });
  }

}
