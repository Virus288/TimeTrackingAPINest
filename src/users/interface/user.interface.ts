export interface User {
  id?: string;
  name: string;
  current_task?: string;
}

// Array of existing elements from interface for validation
export const userData: Array<string> = ["name"]
