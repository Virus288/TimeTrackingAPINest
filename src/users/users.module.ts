// External imports
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

// Internal imports
import { TeamSchema } from '../teams/schema/teams.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Team', schema: TeamSchema }])],
  controllers: [UsersController],
  providers: [UsersService],
})
export class UsersModule {}
