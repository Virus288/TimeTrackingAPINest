import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Put,
  HttpStatus,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { CreateUserDto } from './DTO/create-user.dto';
import { User } from './interface/user.interface';

import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  async get(
    @Res({ passthrough: true }) response: Response,
  ): Promise<User[] | unknown> {
    return this.errorHandler(this.usersService.findAll(), response);
  }

  @Get(':id')
  async getOne(
    @Param('id') id: string,
    @Res({ passthrough: true }) response: Response,
  ): Promise<User | unknown> {
    return this.errorHandler(await this.usersService.findOne(id), response);
  }

  @Post(":id")
  async create(
    @Param('id') id: string,
    @Body() createUserDto: CreateUserDto
  ): Promise<User | unknown> {
    return this.usersService.create(createUserDto, id);
  }

  @Delete(':id')
  async remove(
    @Param('id') id: string,
    @Res({ passthrough: true }) response: Response,
  ): Promise<User | unknown> {
    return this.errorHandler(await this.usersService.delete(id), response);
  }

  @Put(':id')
  async update(
    @Body() createUserDto: CreateUserDto,
    @Param('id') id: string,
    @Res({ passthrough: true }) response: Response,
  ): Promise<User | unknown> {
    return this.errorHandler(
      await this.usersService.update(id, createUserDto),
      response,
    );
  }

  // Handle errors
  errorHandler = (
    data: unknown,
    response: Response<any, Record<string, any>>,
  ) => {
    if (data === null) {
      response.status(HttpStatus.NOT_FOUND).send('Data not found');
    } else {
      return data;
    }
  };
}
