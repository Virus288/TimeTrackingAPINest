import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Please enter username'],
      maxLength: [50, 'Max lenght of username is 50 characters'],
      minLength: [5, 'Min lenght of username is 5 characters'],
    },
    current_task: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Task',
      required: false,
      default: null,
    }
  },
  { timestamps: false },
);
