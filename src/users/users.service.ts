// External imports
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as mongoose from "mongoose";

// Internal imports
import { Team } from '../teams/interface/teams.interface';
import { User } from './interface/user.interface';
import { Task } from '../tasks/interface/tasks.interface';

@Injectable()
export class UsersService {
  private errors: { [key: string]: string } = {};
  constructor(@InjectModel('Team') private readonly teamModel: Model<Team>) {}

  async findAll(): Promise<User[]> {
    try {
      return await this.teamModel.aggregate([ { $unwind: '$users' }])
    } catch (err) {
      return err;
    }
  }

  async findOne(id: string): Promise<User | unknown> {
    try {
      return await this.teamModel.aggregate([ { $unwind: '$users' }, { $match: { "users._id": mongoose.Types.ObjectId(id) } } ])
    } catch (err) {
      return this.getErrors(err, 'oneElement');
    }
  }

  async create(user: User, id: string): Promise<User | unknown> {
    try {
      return await this.teamModel.findOneAndUpdate({ _id: id }, { $push: { users: user } }, { new: true });
    } catch (err) {
      return this.getErrors(err, 'mulipleElements');
    }
  }

  async delete(id: string): Promise<User | unknown> {
    try {
      return await this.teamModel.findOneAndUpdate({}, { $pull: { "users": { _id: id } } }, { new: true });
    } catch (err) {
      return this.getErrors(err, 'oneElement');
    }
  }

  async update(id: string, user: User): Promise<User | unknown> {
    let newData = {}
    Object.keys(user).forEach(key => {
      newData[`users.$.${key}`] = user[key]
    })
    try {
      return await this.teamModel.findOneAndUpdate({ "users._id": id }, { "$set": {...newData} }, { new: true });
    } catch (err) {
      return this.getErrors(err, 'oneElement');
    }
  }

  // Handle errors
  private getErrors = (err: any, from: string) => {
    switch (from) {
      case 'mulipleElements':
        Object.keys(err.errors).forEach((error) => {
          this.addError(error, err.message);
        });
        break;
      case 'oneElement':
        if (err.name === 'CastError') {
          this.addError(
            err.name,
            err.kind +
              ' has wrong format. Make sure that you are providing proper data format',
          );
        }
        break;
      default:
        break;
    }
    return { error: true, data: this.errors };
  };

  // Add error to class variables
  private addError(key: string, val: string) {
    this.errors[key] = val;
  }
}
