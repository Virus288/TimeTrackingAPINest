export interface Task {
  id?: string;
  name: string;
  start_time?: Date;
  finish_time?: Date;
  done?: Boolean;
}

// Array of existing elements from interface for validation
export const taskData: Array<string> = ["name"]
