// External imports
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TasksController } from './tasks.controller';
import { TasksService } from './tasks.service';

// Internal imports
import { TeamSchema } from '../teams/schema/teams.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Team', schema: TeamSchema }])],
  controllers: [TasksController],
  providers: [TasksService],
})
export class TasksModule {}
