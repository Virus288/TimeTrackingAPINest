export class CreateTaskDto {
  readonly id?: string;
  readonly name: string;
  readonly start_time: Date;
  readonly finish_time?: Date;
  readonly done?: boolean
}
