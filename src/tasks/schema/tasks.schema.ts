import * as mongoose from 'mongoose';

export const TaskSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Please enter task name'],
      maxLength: 50,
    },
    start_time: {
      type: Date,
      required: false,
      default: Date.now,
    },
    finish_time: {
      type: Date,
      required: false,
      default: null,
    },
    done: {
      type: Boolean,
      required: false,
      default: false,
    },
  },
  { timestamps: true },
);
