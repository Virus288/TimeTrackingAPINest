import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Put,
  HttpStatus,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { CreateTaskDto } from './DTO/create-tasks.dto';
import { Task } from './interface/tasks.interface';

import { TasksService } from './tasks.service';

@Controller('tasks')
export class TasksController {
  constructor(private readonly taskService: TasksService) {}

  @Get()
  async get(
    @Res({ passthrough: true }) response: Response,
  ): Promise<Task[] | unknown> {
    return this.errorHandler(this.taskService.findAll(), response);
  }

  @Get(':id')
  async getOne(
    @Param('id') id: string,
    @Res({ passthrough: true }) response: Response,
  ): Promise<Task | unknown> {
    return this.errorHandler(await this.taskService.findOne(id), response);
  }

  @Post(":id")
  async create(
    @Param('id') id: string,
    @Body() createTaskDto: CreateTaskDto
  ): Promise<Task | unknown> {
    return this.taskService.create(createTaskDto, id);
  }

  @Delete(':id')
  async remove(
    @Param('id') id: string,
    @Res({ passthrough: true }) response: Response,
  ): Promise<Task | unknown> {
    return this.errorHandler(await this.taskService.delete(id), response);
  }

  @Put(':id')
  async update(
    @Body() createTaskDto: CreateTaskDto,
    @Param('id') id: string,
    @Res({ passthrough: true }) response: Response,
  ): Promise<Task | unknown> {
    return this.errorHandler(
      await this.taskService.update(id, createTaskDto),
      response,
    );
  }

  // Handle errors
  errorHandler = (
    data: unknown,
    response: Response<any, Record<string, any>>,
  ) => {
    if (data === null) {
      response.status(HttpStatus.NOT_FOUND).send('Data not found');
    } else {
      return data;
    }
  };
}
