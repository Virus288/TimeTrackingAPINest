// External imports
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as mongoose from "mongoose";

// Internal imports
import { Team } from '../teams/interface/teams.interface';
import { Task } from './interface/tasks.interface';

@Injectable()
export class TasksService {
  private errors: { [key: string]: string } = {};
  constructor(@InjectModel('Team') private readonly teamModel: Model<Team>) {}

  async findAll(): Promise<Team[]> {
    try {
      return await this.teamModel.aggregate([ { $unwind: '$tasks' }])
    } catch (err) {
      return err;
    }
  }

  async findOne(id: string): Promise<Team | unknown> {
    try {
      return await this.teamModel.aggregate([ { $unwind: '$tasks' }, { $match: { "tasks._id": mongoose.Types.ObjectId(id) } } ])
    } catch (err) {
      return this.getErrors(err, 'oneElement');
    }
  }

  async create(task: Task, id: string): Promise<Team | unknown> {
    try {
      return await this.teamModel.findOneAndUpdate({ _id: id }, { $push: { tasks: task } }, { new: true });
    } catch (err) {
      return this.getErrors(err, 'mulipleElements');
    }
  }

  async delete(id: string): Promise<Task | unknown> {
    try {
      return await this.teamModel.findOneAndUpdate({}, { $pull: { "tasks": { _id: id } } }, { new: true });
    } catch (err) {
      console.log(err)
      return this.getErrors(err, 'oneElement');
    }
  }

  async update(id: string, task: Task): Promise<Task | unknown> {
    let newData = {}
    Object.keys(task).forEach(key => {
      newData[`tasks.$.${key}`] = task[key]
    })
    try {
      return await this.teamModel.findOneAndUpdate({ "tasks._id": id }, { "$set": {...newData} }, { new: true });
    } catch (err) {
      return this.getErrors(err, 'oneElement');
    }
  }

  // Handle errors
  private getErrors = (err: any, from: string) => {
    switch (from) {
      case 'mulipleElements':
        Object.keys(err.errors).forEach((error) => {
          this.addError(error, err.message);
        });
        break;
      case 'oneElement':
        if (err.name === 'CastError') {
          this.addError(
            err.name,
            err.kind +
              ' has wrong format. Make sure that you are providing proper data format',
          );
        }
        break;
      default:
        break;
    }
    return { error: true, data: this.errors };
  };

  // Add error to class variables
  private addError(key: string, val: string) {
    this.errors[key] = val;
  }
}
